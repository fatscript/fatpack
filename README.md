# FATPACK 📦

`fatpack` is a FatScript project file packer utility. This script enables users to consolidate files with specific extensions into a single file, as well as to unpack them. Moreover, it can generate self-extracting-archives for the [FatScript Playground](https://fatscript.org/playground).

## Features

- Produce self-extracting-archives for FatScript Playground.
- Consolidate multiple files into a single file.
- Retain the original directory structure during unpacking.
- Select files based on their extensions.
- Operate as both a CLI tool and a library.

## Getting Started

### Prerequisites

To use `fatpack`, ensure you have `fry` (^2.5.1), the [FatScript](https://fatscript.org) interpreter installed on your system.

### Usage

To employ `fatpack` from the command line:

1. To consolidate a directory:

   ```bash
   fatpack DIR FILE.pack
   ```

2. To generate a self-extracting pack:

   ```bash
   fatpack -z DIR FILE.fpk
   ```

The `-z` parameter is optional for both `.pack` and `.fpk` commands. If provided, `fatpack` will minify FatScript sources. This process is not idempotent, meaning it won't be undone upon unpacking. Regardless of the `-z` parameter all files are compressed with RLE and encoded to Base64 and the unpacking logic will first decode the Base64 blob and then decompress using RLE.

3. To unpack a consolidated file:
   ```bash
   fatpack FILE.pack DIR
   ```

### Installation

Assuming you have `fry`, the FatScript interpreter, installed on your system, setting up this utility is straightforward:

```bash
fry -b $HOME/.local/bin/fatpack fatpack.fat
```

### Alternative installation

Assuming you have `python` installed on your system, there is a simplified version capable of `.fpk` command:

```bash
chmod +x fatpack.py
cp fatpack.py $HOME/.local/bin/fatpack
```

Or simply copy it where needed and run:

```bash
python fatpack.py DIR FILE.fpk
```

> this version is mostly intended for Windows users trying to get their project files into [FatScript Playground](https://fatscript.org/playground), it does not implement RLE compression nor the `-z` option for minifying sources

## Supported Extensions

Currently, the script is compatible with the following file extensions:

- `.fat`
- `.json`
- `.csv`
- `.txt`
- `.md`
- `.conf`
- `.xml`
- `.log`
- `.ppm`
- `.flf`
- `.wav`
- `.mp3`
- `.ogg`

> you can expand the list of permissible extensions in the script as needed

## Ignore file

The scripts support a `.fatignore` file where you can specify one path to ignore per line.

> at the moment only exact filenames are supported (no wildcard or negation rules)

## Notes

- The script will read files as binary and store the data as RLE-Base64 encoded, so the output is web-safe, at the expense of being generally bigger than the input. However fast, RLE compression may not help much with the file size in many cases.
- The script's flexibility allows it to function as a library in other projects or scripts as well.

## Contributing

If you have suggestions for new features or improvements, please open an issue on the [fatpack GitLab](https://gitlab.com/fatscript/fatpack/issues) page.

### Donations

If you find `fatpack` useful and would like to support its development, you can [Buy me a coffee](https://www.buymeacoffee.com/aprates).

## License

[MIT License](LICENSE) © 2024 Antonio Prates.
